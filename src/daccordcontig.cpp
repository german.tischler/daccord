/*
    daccord
    Copyright (C) 2017 German Tischler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <config.h>

// #define HANDLE_INDEL_ESTIMATE_DEBUG
// #define HANDLE_DEBUG
// #define HANDLE_DEEP_DEBUG
// #define CONSENSUS_SERIAL
// #define WINDOWALIGNAVOID
// #define CANDIDATE_AVOID_ALIGN
// #define CONS_HANDLE_SINGLE 0

#include <HandleContext.hpp>
#include <libmaus2/aio/DebugLineOutputStream.hpp>
#include <libmaus2/aio/InputStreamInstance.hpp>
#include <libmaus2/bambam/BamDecoder.hpp>
#include <libmaus2/dazzler/align/OverlapIndexer.hpp>
#include <libmaus2/dazzler/align/AlignmentWriter.hpp>
#include <libmaus2/dazzler/align/OverlapParser.hpp>
#include <libmaus2/dazzler/db/DatabaseFile.hpp>
#include <libmaus2/dazzler/db/InqualContainer.hpp>
#include <libmaus2/fastx/KmerRepeatDetector.hpp>
#include <libmaus2/lcs/AlignerFactory.hpp>
#include <libmaus2/lcs/AlignmentOneAgainstManyFactory.hpp>
#include <libmaus2/lcs/AlignmentPrint.hpp>
#include <libmaus2/lcs/NNP.hpp>
#include <libmaus2/lcs/SuffixArrayLCS.hpp>
#include <libmaus2/math/binom.hpp>
#include <libmaus2/math/Convolution.hpp>
#include <libmaus2/math/ipow.hpp>
#include <libmaus2/parallel/LockedGrowingFreeList.hpp>
#include <libmaus2/parallel/NumCpus.hpp>
#include <libmaus2/parallel/SynchronousCounter.hpp>
#include <libmaus2/rank/ERank222B.hpp>
#include <libmaus2/rmq/QuickDynamicRMQ.hpp>
#include <libmaus2/timing/RealTimeClock.hpp>
#include <libmaus2/util/ArgInfo.hpp>
#include <libmaus2/util/ArgParser.hpp>
#include <libmaus2/util/FiniteSizeHeap.hpp>
#include <libmaus2/util/PrefixSums.hpp>
#include <libmaus2/util/MemUsage.hpp>
#include <libmaus2/util/TempFileRemovalContainer.hpp>
#include <libmaus2/wavelet/WaveletTree.hpp>

#if defined(HANDLE_INDEL_ESTIMATE_DEBUG) || defined(HANDLE_DEBUG)
#include <libmaus2/fastx/StreamFastAReader.hpp>
#include <libmaus2/lcs/NP.hpp>
#endif

std::string getTmpFileBase(libmaus2::util::ArgParser const & arg)
{
	std::string const tmpfilebase = arg.uniqueArgPresent("T") ? arg["T"] : libmaus2::util::ArgInfo::getDefaultTmpFileName(arg.progname);
	return tmpfilebase;
}


struct OverlapTypeInfo
{
	typedef libmaus2::dazzler::align::Overlap element_type;
	typedef element_type::shared_ptr_type pointer_type;

	static pointer_type getNullPointer()
	{
		return pointer_type();
	}

	static pointer_type deallocate(pointer_type /* p */)
	{
		return getNullPointer();
	}
};

struct OverlapAllocator
{
	typedef libmaus2::dazzler::align::Overlap element_type;
	typedef element_type::shared_ptr_type pointer_type;

	pointer_type operator()() const
	{
		return pointer_type(new element_type);
	}
};

static uint64_t getDefaultNumThreads()
{
	return libmaus2::parallel::NumCpus::getNumLogicalProcessors();
}

static uint64_t getDefaultVerbose()
{
	return std::numeric_limits<uint64_t>::max();
}

#if 0
static uint64_t getDefaultK()
{
	return 8;
}
#endif

static uint64_t getDefaultMaxInput()
{
	return 5000;
}

#if 0
static uint64_t getDefaultVarD()
{
	return 0;
}
#endif

static uint64_t getDefaultMaxAlign()
{
	return std::numeric_limits<uint64_t>::max();
}

static unsigned int getDefaultWindowSize()
{
	return 40;
}

static unsigned int getDefaultAdvanceSize()
{
	return 10;
}

static unsigned int getDefaultProduceFull()
{
	return 0;
}

static unsigned int getDefaultMinWindowCoverage()
{
	return 3;
}

static uint64_t getDefaultMinWindowError()
{
	return std::numeric_limits<uint64_t>::max();
}

static uint64_t getDefaultMinLen()
{
	return 0;
}

static uint64_t getDefaultMinFilterFreq()
{
	return 0;
}

static uint64_t getDefaultMaxFilterFreq()
{
	return 2;
}

template<typename default_type>
static std::string formatRHS(std::string const & description, default_type def)
{
	std::ostringstream ostr;
	ostr << description << " (default " << def << ")";
	return ostr.str();
}

/*
 parameters:

 -t : default number of logical cores, threads
 */

static std::string helpMessage(libmaus2::util::ArgParser const & arg)
{
	std::vector < std::pair < std::string, std::string > > optionMap;
	optionMap . push_back ( std::pair < std::string, std::string >("t", formatRHS("number of threads",getDefaultNumThreads())));
	optionMap . push_back ( std::pair < std::string, std::string >("w", formatRHS("window size",getDefaultWindowSize())));
	optionMap . push_back ( std::pair < std::string, std::string >("a", formatRHS("advance size",getDefaultAdvanceSize())));
	optionMap . push_back ( std::pair < std::string, std::string >("d", formatRHS("max depth",getDefaultMaxAlign())));
	optionMap . push_back ( std::pair < std::string, std::string >("f", formatRHS("produce full sequences",getDefaultProduceFull())));
	optionMap . push_back ( std::pair < std::string, std::string >("V", formatRHS("verbosity",getDefaultVerbose())));
	optionMap . push_back ( std::pair < std::string, std::string >("I", formatRHS("read interval",std::string("0,") + libmaus2::util::NumberSerialisation::formatNumber(std::numeric_limits<uint64_t>::max(),0))));
	optionMap . push_back ( std::pair < std::string, std::string >("J", formatRHS("reads part",std::string("0,1"))));
	optionMap . push_back ( std::pair < std::string, std::string >("E", formatRHS("error profile file name",std::string("input.las.eprof"))));
	optionMap . push_back ( std::pair < std::string, std::string >("m", formatRHS("minimum window coverage",getDefaultMinWindowCoverage())));
	optionMap . push_back ( std::pair < std::string, std::string >("e", formatRHS("maximum window error",getDefaultMinWindowError())));
	optionMap . push_back ( std::pair < std::string, std::string >("l", formatRHS("minimum length of output",getDefaultMinLen())));
	optionMap . push_back ( std::pair < std::string, std::string >("minfilterfreq", formatRHS("minimum k-mer filter frequency",getDefaultMinFilterFreq())));
	optionMap . push_back ( std::pair < std::string, std::string >("maxfilterfreq", formatRHS("maximum k-mer filter frequency",getDefaultMaxFilterFreq())));
	optionMap . push_back ( std::pair < std::string, std::string >("T", formatRHS("temporary file prefix",libmaus2::util::ArgInfo::getDefaultTmpFileName(arg.progname))));
	optionMap . push_back ( std::pair < std::string, std::string >("D", formatRHS("maximum number of alignments considered per read",getDefaultMaxInput())));

	uint64_t maxlhs = 0;
	for ( std::vector < std::pair < std::string, std::string > >::const_iterator ita = optionMap.begin(); ita != optionMap.end(); ++ita )
	{
		assert ( ita->first.size() );

		if ( ita->first.size() == 1 )
			maxlhs = std::max(maxlhs,static_cast<uint64_t>(ita->first.size()+1));
		else
			maxlhs = std::max(maxlhs,static_cast<uint64_t>(ita->first.size()+2));
	}

	std::ostringstream messtr;
	for ( std::vector < std::pair < std::string, std::string > >::const_iterator ita = optionMap.begin(); ita != optionMap.end(); ++ita )
	{
		std::string const key = ita->first;

		messtr << "\t";
		messtr << std::setw(maxlhs) << std::setfill(' ');
		if ( key.size() == 1 )
			messtr << (std::string("-")+key);
		else
			messtr << (std::string("--")+key);

		messtr << std::setw(0);

		messtr << ": ";

		messtr << ita->second;
		messtr << "\n";
	}

	return messtr.str();
}

#if defined(HANDLE_INDEL_ESTIMATE_DEBUG) || defined(HANDLE_DEBUG)
// load reference text from a FastA file
static std::vector<std::string> loadTextVector(std::string const & textfn)
{
	libmaus2::aio::InputStreamInstance ISI(textfn);
	libmaus2::fastx::StreamFastAReaderWrapper SFARW(ISI);
	libmaus2::fastx::StreamFastAReaderWrapper::pattern_type pattern;
	std::vector<std::string> Vtext;

	while ( SFARW.getNextPatternUnlocked(pattern) )
		Vtext.push_back(pattern.spattern);

	return Vtext;
}

// load all alignments from a BAM file
std::vector<libmaus2::bambam::BamAlignment::shared_ptr_type> readBamAlignments(std::string const & bamfn)
{
	std::vector<libmaus2::bambam::BamAlignment::shared_ptr_type> V;
	libmaus2::bambam::BamDecoder bamdec(bamfn);
	libmaus2::bambam::BamAlignment const & algn = bamdec.getAlignment();
	while ( bamdec.readAlignment() )
		V.push_back(algn.sclone());
	return V;
}
#endif


template<unsigned int k>
double handleIndelEstimate(
	std::ostream &
		#if defined(HANDLE_INDEL_ESTIMATE_DEBUG)
		err
		#endif
		,
	uint64_t const maxalign,
	#if 0
	// overlaps
	std::vector<libmaus2::dazzler::align::Overlap>::const_iterator ita,
	std::vector<libmaus2::dazzler::align::Overlap>::const_iterator ite,
	#endif
	libmaus2::dazzler::align::Overlap const * ita,
	libmaus2::dazzler::align::Overlap const * ite,
	// window size for consensus computation
	uint64_t const windowsize,
	uint64_t const advancesize,
	// reads
	DecodedReadContainer & RC,
	DecodedReadContainer & RC2,
	// trace free list
	libmaus2::parallel::LockedGrowingFreeList<trace_type,TraceAllocator,TraceTypeInfo> & traceFreeList,
	// aligner
	libmaus2::lcs::Aligner & NP,
	// trace point space for overlaps
	int64_t const tspace,
	// kmer length
	// unsigned int const k,
	libmaus2::lcs::AlignmentStatistics & RGAS,
	uint64_t & usable,
	uint64_t & unusable
	#if defined(HANDLE_INDEL_ESTIMATE_DEBUG)
	,
	// BAM alignment of OVL-A read to reference
	libmaus2::bambam::BamAlignment const & algn,
	// reference text
	std::string const & text,
	//
	libmaus2::lcs::AlignmentStatistics & TAS,
	libmaus2::parallel::StdSpinLock & TASlock
	#endif
)
{
	// number of overlaps with A read
	uint64_t const nintv = ite-ita;

	double maxerate = 0.0;
	double minerate = 1.0;
	for ( uint64_t i = 0; i < nintv; ++i )
	{
		double const erate = ita[i].getErrorRate();
		if ( erate > maxerate )
			maxerate = erate;
		if ( erate < minerate )
			minerate = erate;
	}
	double const ediv = (maxerate > minerate) ? (maxerate - minerate) : 1.0;

	int64_t const aread = nintv ? ita[0].aread : -1;
	for ( uint64_t i = 1; i < nintv; ++i )
		assert ( ita[i].aread == aread );

	typedef std::pair<uint64_t,uint64_t> upair;

	#if defined(HANDLE_INDEL_ESTIMATE_DEBUG)
	// cig op vector
	libmaus2::autoarray::AutoArray<libmaus2::bambam::cigar_operation> cigop;
	// number of cigar operations in ground truth alignment
	uint32_t const ncigar = algn.getCigarOperations(cigop);
	// get trace from free list
	trace_type::shared_ptr_type Pbamtrace = traceFreeList.get();
	// turn cigar of reference/ground truth to trace
	libmaus2::bambam::CigarStringParser::cigarToTrace(cigop.begin(),cigop.begin()+ncigar,*Pbamtrace,true /* ignore unknown */);
	// part of text covered by read
	bool const bamrev = algn.isReverse();
	std::string const psreftext = text.substr(algn.getPos()-algn.getFrontDel(),algn.getReferenceLength());
	// reference text matched by A read
	std::string const sreftext = bamrev ? libmaus2::fastx::reverseComplementUnmapped(psreftext) : psreftext;
	// get start and end pointer
	libmaus2::lcs::AlignmentTraceContainer::step_type * rta = Pbamtrace->ta;
	libmaus2::lcs::AlignmentTraceContainer::step_type * rte = Pbamtrace->te;
	// reverse vector if we are on the reverse complement
	if ( bamrev )
		std::reverse(rta,rte);
	// pointer to reference string
	uint8_t const * refu = reinterpret_cast<uint8_t const *>(sreftext.c_str());
	// front soft clipping
	uint64_t frontsoftclip = algn.getFrontSoftClipping();

	{
		std::string const R = algn.getRead();
		uint8_t const * ru = reinterpret_cast<uint8_t const *>(R.c_str()) + algn.getFrontSoftClipping();
		libmaus2::lcs::NP NP;
		NP.np(
			reinterpret_cast<uint8_t const *>(psreftext.c_str()),
			reinterpret_cast<uint8_t const *>(psreftext.c_str())+psreftext.size(),
			ru,ru + (R.size() - (algn.getFrontSoftClipping() + algn.getBackSoftClipping()))
		);
		libmaus2::lcs::AlignmentStatistics const AS = NP.getAlignmentStatistics();

		{
			libmaus2::parallel::ScopeStdSpinLock slock(TASlock);
			TAS += AS;
		}

		// std::cerr << "minimal " << AS << std::endl;

		#if 0
		libmaus2::lcs::AlignmentStatistics const ASe = Pbamtrace->getAlignmentStatistics();
		std::cerr << "actual " << ASe << std::endl;
		#endif
	}
	#endif

	// traces
	std::map<uint64_t,trace_type::shared_ptr_type> Mtraces;

	// repeat detector for length k-1
	libmaus2::fastx::KmerRepeatDetector KRD(k-1);

	// compute traces
	uint64_t maxaepos = 0;

	if ( nintv )
	{
		uint8_t const * ua = reinterpret_cast<uint8_t const *>(RC.getForwardRead(ita[0].aread));

		for ( uint64_t z = 0; z < nintv; ++z )
		{
			// get trace from free list
			trace_type::shared_ptr_type Ptrace = traceFreeList.get();

			// update maximum aepos
			if ( ita[z].path.aepos > static_cast<int64_t>(maxaepos) )
				maxaepos = ita[z].path.aepos;

			// get pointers to data
			uint8_t const * ub = reinterpret_cast<uint8_t const *>(ita[z].isInverse() ? RC2.getReverseComplementRead(ita[z].bread) : RC2.getForwardRead(ita[z].bread));

			// compute the trace operations
			ita[z].computeTrace(ua,ub,tspace,*Ptrace,NP);

			// store trace object
			Mtraces[z] = Ptrace;
		}
	}

	// end of interval heap
	libmaus2::util::FiniteSizeHeap< std::pair<uint64_t,uint64_t> > E(1024);
	// set of active traces/reads
	std::map < uint64_t, ActiveElement > activeset;
	libmaus2::autoarray::AutoArray < std::pair< uint8_t const *, uint64_t> > MA;

	// de bruijn graph for k
	KmerLimit KL(0.85,0);
	DebruijnGraph<k> DG(0,KL);

	// insert pointer
	uint64_t z = 0;

	uint64_t const ylimit = (maxaepos + advancesize >= windowsize) ? ((maxaepos + advancesize - windowsize) / advancesize) : 0;

	assert ( ylimit * advancesize + windowsize > maxaepos );

	double esum = 0;
	uint64_t ecnt = 0;

	// window id
	for ( uint64_t y = 0; y < ylimit; ++y )
	{
		// start of window on A
		uint64_t const astart = y * advancesize;
		uint64_t const aend = astart + windowsize;
		assert ( aend <= maxaepos );

		#if defined(HANDLE_INDEL_ESTIMATE_DEBUG)
		// bases used on reference
		uint64_t const refcov = frontsoftclip >= windowsize ? 0 : windowsize - frontsoftclip;

		// part of reference not handled by front soft clipping
		std::pair<uint64_t,uint64_t> const advref = libmaus2::lcs::AlignmentTraceContainer::advanceB(rta,rte,refcov);
		assert ( advref.first == refcov || rta + advref.second == rte );
		std::pair<uint64_t,uint64_t> const slref = libmaus2::lcs::AlignmentTraceContainer::getStringLengthUsed(rta,rta+advref.second);
		#endif

		// add new active intervals
		while ( z < nintv && static_cast<int64_t>(astart) >= ita[z].path.abpos )
		{
			if ( ita[z].path.aepos >= static_cast<int64_t>(astart) )
			{
				uint64_t const aoff = astart - ita[z].path.abpos;
				// err << "aoff=" << aoff << " ab=" << ita[z].path.abpos << std::endl;

				assert ( (ita[z].path.abpos + aoff) % advancesize == 0 );

				// get trace
				trace_type::shared_ptr_type Ptrace = Mtraces.find(z)->second;
				libmaus2::lcs::AlignmentTraceContainer::step_type const * ta = Ptrace->ta;
				libmaus2::lcs::AlignmentTraceContainer::step_type const * te = Ptrace->te;

				// see how many operations there are up to start of region we are interested in
				std::pair<uint64_t,uint64_t> const adv = libmaus2::lcs::AlignmentTraceContainer::advanceA(ta,te,aoff);
				assert ( adv.first == aoff );

				uint8_t const * ua = reinterpret_cast<uint8_t const *>(RC.getForwardRead(ita[z].aread)) + ita[z].path.abpos + aoff;
				uint64_t const uboff = ita[z].path.bbpos + libmaus2::lcs::AlignmentTraceContainer::getStringLengthUsed(ta,ta+adv.second).second;
				uint8_t const * ub = reinterpret_cast<uint8_t const *>(ita[z].isInverse() ? RC2.getReverseComplementRead(ita[z].bread) : RC2.getForwardRead(ita[z].bread)) + uboff;

				// advance in trace
				ta += adv.second;

				uint64_t const escore = static_cast<uint64_t>(((ita[z].getErrorRate() - minerate) / ediv) * std::numeric_limits<uint32_t>::max());
				assert ( escore <= std::numeric_limits<uint32_t>::max() );
				uint64_t const eindex = (escore<<32) | z;

				activeset[eindex] = ActiveElement(ua,ub,ta,te,uboff,ita[z].getErrorRate());
				// add to erase list
				E.pushBump(upair(ita[z].path.aepos,eindex));
			}

			z += 1;
		}
		// cleanup
		while ( (! (E.empty())) && E.top().first <= aend )
		{
			upair UP = E.pop();
			activeset.erase(UP.second);
		}

		uint64_t MAo = 0;

		// iterate over active read list
		for ( std::map<uint64_t,ActiveElement>::iterator s_ita = activeset.begin(); s_ita != activeset.end(); ++s_ita )
		{
			// active element
			ActiveElement & AE = s_ita->second;

			#if ! defined(NDEBUG)
			// read id
			uint64_t const key = s_ita->first & std::numeric_limits<uint32_t>::max();
			// get overlap
			libmaus2::dazzler::align::Overlap const & OVL = ita[key];
			// sanity checks
			//std::cerr << "check " << OVL << std::endl;
			assert ( OVL.path.abpos <= static_cast<int64_t>(astart ) );
			assert ( OVL.path.aepos >= static_cast<int64_t>(aend   ) );
			#endif

			// get end of region in trace
			std::pair<uint64_t,uint64_t> const adv = libmaus2::lcs::AlignmentTraceContainer::advanceA(AE.ta,AE.te,windowsize);
			assert ( adv.first == windowsize );
			// string length
			std::pair<uint64_t,uint64_t> const sl = libmaus2::lcs::AlignmentTraceContainer::getStringLengthUsed(AE.ta,AE.ta+adv.second);

			// push A read if this is the first instance of this loop
			if ( (! MAo) && (&RC != &RC2) )
				MA.push(MAo,std::pair< uint8_t const *, uint64_t>(AE.ua,windowsize));
			// push B read
			if ( MAo < maxalign )
				MA.push(MAo,std::pair< uint8_t const *, uint64_t>(AE.ub,sl.second));

			std::pair<uint64_t,uint64_t> const advadv = libmaus2::lcs::AlignmentTraceContainer::advanceA(AE.ta,AE.te,advancesize);
			std::pair<uint64_t,uint64_t> const sladv = libmaus2::lcs::AlignmentTraceContainer::getStringLengthUsed(AE.ta,AE.ta+advadv.second);

			// update active element by advancing to next window
			AE.ua += advancesize;
			AE.ta += advadv.second;
			AE.ub += sladv.second;
			AE.uboff += sladv.second;
		}

		if ( MAo >= 3 )
		{
			bool ghasrep = false;

			for ( uint64_t i = 0; i < MAo; ++i )
			{
				// does read have a repeating k-1 mer?
				bool const hasrep = KRD.detect(MA[i].first,MA[i].second);
				ghasrep = ghasrep || hasrep;
			}

			if ( ghasrep )
			{
				unusable += 1;
			}
			else
			{
				usable += 1;

				#if defined(HANDLE_INDEL_ESTIMATE_DEBUG)
				// check whether ground truth has repeating k-1 mer
				bool const refhasrep = KRD.detect(refu,slref.first);
				// err << "ghasrep=" << ghasrep << " refhasrep=" << refhasrep << std::endl;
				#endif

				// set up debruijn graph
				DG.setup(MA.begin(), MAo);
				// filter by freuency (need at least 2)
				DG.filterFreq(2,MAo);
				// DG.computeFeasibleKmerPositions(OffsetLikely const & offsetLikely, double const thres = 1e-3)
				//DB.check();

				// traverse the graph
				bool const consok = DG.traverseTrivial();

				#if defined(HANDLE_INDEL_ESTIMATE_DEBUG)
				if ( ghasrep != refhasrep )
				{
					err << "GHASREP != REFHASREP for aread = " << ita[0].aread << " " << consok << std::endl;
					KRD.printRepeats();
				}
				#endif

				// if consensus looks ok
				if ( consok )
				{
					// get the consensus
					std::string const consensus = DG.getConsensus();

					libmaus2::lcs::AlignmentStatistics GAS;

					// align all reads to the consensus
					for ( uint64_t i = 0; i < MAo; ++i )
					{
						NP.align(
							reinterpret_cast<uint8_t const *>(consensus.c_str()),
							consensus.size(),
							MA[i].first,
							MA[i].second
						);

						// update error profile
						GAS += NP.getTraceContainer().getAlignmentStatistics();
					}

					RGAS += GAS;

					esum += GAS.getErrorRate();
					ecnt += 1;
				}
			}
		}

		#if defined(HANDLE_INDEL_ESTIMATE_DEBUG)
		uint64_t const refadv = frontsoftclip >= advancesize ? 0 : advancesize - frontsoftclip;
		std::pair<uint64_t,uint64_t> const advrefadv = libmaus2::lcs::AlignmentTraceContainer::advanceB(rta,rte,refadv);
		std::pair<uint64_t,uint64_t> const slrefadv = libmaus2::lcs::AlignmentTraceContainer::getStringLengthUsed(rta,rta+advrefadv.second);

		frontsoftclip -= std::min(frontsoftclip,advancesize);

		rta += advrefadv.second;
		refu += slrefadv.first;
		#endif
	}

	for ( std::map<uint64_t,trace_type::shared_ptr_type>::const_iterator m_ita =
		Mtraces.begin(); m_ita != Mtraces.end(); ++m_ita )
		traceFreeList.put(m_ita->second);
	#if defined(HANDLE_INDEL_ESTIMATE_DEBUG)
	traceFreeList.put(Pbamtrace);
	#endif

	return ecnt ? (esum / ecnt) : 0.0;
}



struct OverlapPosComparator
{
	bool operator()(libmaus2::dazzler::align::Overlap const & lhs, libmaus2::dazzler::align::Overlap const & rhs) const
	{
		return lhs.path.abpos < rhs.path.abpos;
	}
};

struct OverlapErrorDescComparator
{
	bool operator()(libmaus2::dazzler::align::Overlap const & lhs, libmaus2::dazzler::align::Overlap const & rhs) const
	{
		return lhs.getErrorRate() < rhs.getErrorRate();
	}
};


struct ErrorInfo
{
	typedef ErrorInfo this_type;
	typedef std::unique_ptr<this_type> unique_ptr_type;
	typedef std::shared_ptr<this_type> shared_ptr_type;

	libmaus2::lcs::AlignmentStatistics AS;

	ErrorInfo()
	{

	}
	ErrorInfo(libmaus2::lcs::AlignmentStatistics const & rAS)
	: AS(rAS)
	{

	}
	ErrorInfo(std::istream & in)
	{
		deserialise(in);
	}
	ErrorInfo(std::string const & fn)
	{
		deserialise(fn);
	}

	std::ostream & serialise(std::ostream & out) const
	{
		return AS.serialise(out);
	}

	void serialise(std::string const & fn) const
	{
		return AS.serialise(fn);
	}

	std::istream & deserialise(std::istream & in)
	{
		return AS.deserialise(in);
	}

	void deserialise(std::string const & fn)
	{
		AS.deserialise(fn);
	}
};

int daccordcontig(
	libmaus2::util::ArgParser const & arg
	#if defined(HANDLE_INDEL_ESTIMATE_DEBUG) || defined(HANDLE_DEBUG)
		,
	std::string const & textfn,
	std::string const & bamfn
	#endif
)
{
	std::string const tmpprefix = getTmpFileBase(arg);
	uint64_t const numthreads = arg.uniqueArgPresent("t") ? arg.getUnsignedNumericArg<uint64_t>("t") : getDefaultNumThreads();
	uint64_t const maxalign = arg.uniqueArgPresent("maxalign") ? arg.getUnsignedNumericArg<uint64_t>("maxalign") : std::numeric_limits<uint64_t>::max();

	std::vector<std::string> Vtmp(numthreads);
	libmaus2::autoarray::AutoArray < libmaus2::aio::OutputStreamInstance::unique_ptr_type > Aout(numthreads);
	for ( uint64_t i = 0; i < numthreads; ++i )
	{
		std::ostringstream ostr;
		ostr << tmpprefix << "_" << std::setw(6) << std::setfill('0') << i;
		Vtmp[i] = ostr.str();
		libmaus2::util::TempFileRemovalContainer::addTempFile(Vtmp[i]);

		libmaus2::aio::OutputStreamInstance::unique_ptr_type tptr(
			new libmaus2::aio::OutputStreamInstance(Vtmp[i])
		);
		Aout[i] = std::move(tptr);
	}

	std::string const db0fn = arg[0];
	libmaus2::dazzler::db::DatabaseFile DB0(db0fn);
	if ( DB0.part != 0 )
	{
		libmaus2::exception::LibMausException lme;
		lme.getStream() << "Partial databases are not supported." << std::endl;
		lme.finish();
		throw lme;
	}
	DB0.computeTrimVector();
	std::vector<uint64_t> RL0;
	DB0.getAllReadLengths(RL0);

	std::string const db1fn = arg[1];
	libmaus2::dazzler::db::DatabaseFile DB1(db1fn);
	if ( DB1.part != 0 )
	{
		libmaus2::exception::LibMausException lme;
		lme.getStream() << "Partial databases are not supported." << std::endl;
		lme.finish();
		throw lme;
	}
	DB1.computeTrimVector();
	std::vector<uint64_t> RL1;
	DB1.getAllReadLengths(RL1);

	std::string const lasfn = arg[2];
	int64_t const tspace = libmaus2::dazzler::align::AlignmentFile::getTSpace(lasfn);
	// bool const small = libmaus2::dazzler::align::AlignmentFile::tspaceToSmall(tspace);

	libmaus2::parallel::LockedGrowingFreeList<trace_type,TraceAllocator,TraceTypeInfo> traceFreeList;
	libmaus2::parallel::LockedGrowingFreeList<ReadData,ReadDataAllocator,ReadDataTypeInfo> readDataFreeList;

	ReadDecoderAllocator RD0(&DB0);
	libmaus2::parallel::LockedGrowingFreeList<ReadDecoder,ReadDecoderAllocator,ReadDecoderTypeInfo>::unique_ptr_type PreadDecoderFreeList0(
		new libmaus2::parallel::LockedGrowingFreeList<ReadDecoder,ReadDecoderAllocator,ReadDecoderTypeInfo>(RD0)
	);
	libmaus2::parallel::LockedGrowingFreeList<ReadDecoder,ReadDecoderAllocator,ReadDecoderTypeInfo> & readDecoderFreeList0 = *PreadDecoderFreeList0;

	ReadDecoderAllocator RD1(&DB1);
	libmaus2::parallel::LockedGrowingFreeList<ReadDecoder,ReadDecoderAllocator,ReadDecoderTypeInfo>::unique_ptr_type PreadDecoderFreeList1(
		new libmaus2::parallel::LockedGrowingFreeList<ReadDecoder,ReadDecoderAllocator,ReadDecoderTypeInfo>(RD1)
	);
	libmaus2::parallel::LockedGrowingFreeList<ReadDecoder,ReadDecoderAllocator,ReadDecoderTypeInfo> & readDecoderFreeList1 = *PreadDecoderFreeList1;

	libmaus2::parallel::SynchronousCounter<uint64_t> wellcounter;
	libmaus2::parallel::SynchronousCounter<uint64_t> idcounter;

	uint64_t const handle_maxalign = maxalign;
	uint64_t const handle_windowsize = 40;
	uint64_t const handle_advancesize = 10;
	uint64_t const handle_tspace = tspace;

	std::string const eproffn = arg.uniqueArgPresent("E") ? arg["E"] : (lasfn + ".eprof");

	if (
		(!libmaus2::util::GetFileSize::fileExists(eproffn))
	)
	{
		libmaus2::exception::LibMausException lme;
		lme.getStream() << "[V] unable to find eprof file " << eproffn << std::endl;
		lme.finish();
		throw lme;
	}

	libmaus2::lcs::AlignmentStatistics GAS;
	{
		libmaus2::aio::InputStreamInstance ISI(eproffn);
		GAS.deserialise(ISI);
	}

	uint64_t const len = GAS.matches + GAS.mismatches + GAS.deletions;
	uint64_t const numerr = GAS.mismatches + GAS.deletions + GAS.insertions;

	double const est_erate = static_cast<double>(numerr) / len;
	double const est_cor = 1.0 - est_erate;
	double const est_i_frac = GAS.insertions / static_cast<double>(numerr);
	double const est_d_frac = GAS.deletions / static_cast<double>(numerr);
	double const est_s_frac = GAS.mismatches / static_cast<double>(numerr);

	double const p_i = static_cast<double>(GAS.insertions) / len;
	double const p_d = static_cast<double>(GAS.deletions) / len;
	double const p_s = static_cast<double>(GAS.mismatches) / len;

	std::cerr << GAS << std::endl;
	std::cerr << "len=" << len << std::endl;
	std::cerr << "error estimates" << std::endl;
	std::cerr << "erate=" << est_erate << std::endl;
	std::cerr << "cor=" << est_cor << std::endl;
	std::cerr << "ins=" << est_i_frac << " " << p_i << std::endl;
	std::cerr << "del=" << est_d_frac << " " << p_d << std::endl;
	std::cerr << "subst=" << est_s_frac << " " << p_s << std::endl;

	#if 0
	double const erate = est_erate;
	//double const est_cor = 1.0-erate;
	double const delrate = est_d_frac;
	double const insrate = est_i_frac;
	#endif
	#if 0
	double const substrate = 1 - delrate - insrate;
	double const inshomopolrate = 0;
	double const eratelow = erate;
	double const eratehigh = erate;
	double const eratelowstddev = 0;
	double const eratehighstddev = 0;
	double const keeplowstate = 1;
	double const keephighstate = 0;
	double const startlowprob = 1;
	double const pp = 1.0 + p_i - p_d;
	#endif

	OffsetLikely const offsetLikely = computeOffsetLikely(handle_windowsize,p_i,p_d);
	bool const handle_producefull = true;
	int const handle_verbose = 0;
	uint64_t const handle_minwindowcov = 2;
	uint64_t const handle_eminrate = std::numeric_limits<uint64_t>::max();
	uint64_t const handle_minlen = 0;
	int64_t const handle_minfilterfreq = 0;
	int64_t const handle_maxfilterfreq = 2;

	uint64_t handle_kmersizelow = 6;
	uint64_t handle_kmersizehigh = 8;

	if ( arg.uniqueArgPresent("k") )
	{
		std::string const karg = arg["k"];

		if ( karg.find(",") != std::string::npos )
		{
			std::istringstream istr(karg);
			istr >> handle_kmersizelow;
			if ( (!istr) || istr.peek() != ',' )
			{
				libmaus2::exception::LibMausException lme;
				lme.getStream() << "[E] unable to parse k argument " << karg << std::endl;
				lme.finish();
				throw lme;
			}
			istr.get(); // ','
			istr >> handle_kmersizehigh;
			if ( (!istr) || istr.peek() != std::istream::traits_type::eof() )
			{
				libmaus2::exception::LibMausException lme;
				lme.getStream() << "[E] unable to parse k argument " << karg << std::endl;
				lme.finish();
				throw lme;
			}
		}
		else
		{
			std::istringstream istr(karg);
			istr >> handle_kmersizelow;

			if ( (!istr) || istr.peek() != std::istream::traits_type::eof() )
			{
				libmaus2::exception::LibMausException lme;
				lme.getStream() << "[E] unable to parse k argument " << karg << std::endl;
				lme.finish();
				throw lme;
			}

			handle_kmersizehigh = handle_kmersizelow;
		}
	}

	std::cerr << "[V] using kmer range [" << handle_kmersizelow << "," << handle_kmersizehigh << "]" << std::endl;

	std::map < uint64_t, KmerLimit::shared_ptr_type > MKL;
	for ( uint64_t k = handle_kmersizelow; k <= handle_kmersizehigh; ++k )
	{
		KmerLimit::shared_ptr_type tptr(new KmerLimit(::std::pow(est_cor,k),100));
		MKL [ k ] = tptr;
	}

	// consensus contexts
	HandleContext::unique_ptr_type hcontext(
		new HandleContext(
			handle_maxalign,
			wellcounter,
			handle_windowsize,
			handle_advancesize,
			readDataFreeList,
			readDecoderFreeList0,
			readDecoderFreeList1,
			traceFreeList,
			handle_tspace,
			offsetLikely,
			handle_producefull,
			est_cor,
			handle_kmersizelow,
			handle_kmersizehigh,
			handle_verbose,
			handle_minwindowcov,
			handle_eminrate,
			handle_minlen,
			handle_minfilterfreq,
			handle_maxfilterfreq,
			MKL,
			numthreads/*numthreads*/
		)
	);

	arg.printArgs(std::cerr,std::string("[V] "));

	int64_t minaread = 0;
	int64_t maxaread = static_cast<int64_t>(RL0.size())-1;

	if ( arg.uniqueArgPresent("J") )
	{

		std::string const Js = arg["J"];
		std::istringstream istr(Js);
		int64_t Icnt;
		istr >> Icnt;

		if ( ! istr )
		{
			libmaus2::exception::LibMausException lme;
			lme.getStream() << "[E] unable to parse " << Js << std::endl;
			lme.finish();
			throw lme;
		}

		int const c = istr.get();

		if ( ! istr || c == std::istream::traits_type::eof() || c != ',' )
		{
			libmaus2::exception::LibMausException lme;
			lme.getStream() << "[E] unable to parse " << Js << std::endl;
			lme.finish();
			throw lme;
		}

		int64_t Idiv;
		istr >> Idiv;

		if ( ! istr || istr.peek() != std::istream::traits_type::eof() )
		{
			libmaus2::exception::LibMausException lme;
			lme.getStream() << "[E] unable to parse " << Js << std::endl;
			lme.finish();
			throw lme;
		}

		int64_t const toparead = maxaread + 1;
		int64_t const readspan = (toparead > minaread) ? (toparead-minaread) : 0;

		if ( readspan && ! Idiv )
		{
			libmaus2::exception::LibMausException lme;
			lme.getStream() << "[E] denominator of J argument cannot be zero" << std::endl;
			lme.finish();
			throw lme;
		}

		if ( toparead > minaread )
		{
			int64_t const partsize = Idiv ? (readspan + Idiv - 1)/Idiv : 0;

			int64_t const ilow = std::min(minaread + Icnt * partsize,toparead);
			int64_t const ihigh = std::min(ilow+partsize,toparead);

			if ( ihigh > ilow )
			{
				minaread = ilow;
				maxaread = ihigh-1;
			}
			else
			{
				minaread = 0;
				maxaread = -1;
			}
		}
	}
	else if ( arg.uniqueArgPresent("I") )
	{
		std::string const Is = arg["I"];
		std::istringstream istr(Is);
		int64_t Iminaread;
		istr >> Iminaread;

		if ( ! istr )
		{
			libmaus2::exception::LibMausException lme;
			lme.getStream() << "[E] unable to parse " << Is << std::endl;
			lme.finish();
			throw lme;
		}

		int const c = istr.get();

		if ( ! istr || c == std::istream::traits_type::eof() || c != ',' )
		{
			libmaus2::exception::LibMausException lme;
			lme.getStream() << "[E] unable to parse " << Is << std::endl;
			lme.finish();
			throw lme;
		}

		int64_t Imaxaread;
		istr >> Imaxaread;

		if ( ! istr || istr.peek() != std::istream::traits_type::eof() )
		{
			libmaus2::exception::LibMausException lme;
			lme.getStream() << "[E] unable to parse " << Is << std::endl;
			lme.finish();
			throw lme;
		}

		minaread = std::max(Iminaread,minaread);
		maxaread = std::min(Imaxaread,maxaread);
	}


	libmaus2::dazzler::align::BinIndexDecoder const binindex(lasfn);
	libmaus2::timing::RealTimeClock rtc;
	for ( int64_t aread = minaread; aread <= maxaread; ++aread )
	{
		rtc.start();
		(*hcontext)(std::cout,std::cerr,lasfn,binindex,aread);
		std::cerr << "[V] processed contig " << aread << " in time " << rtc.formatTime(rtc.getElapsedSeconds()) << std::endl;
	}

	return EXIT_SUCCESS;
}

int main(int argc, char * argv[])
{
	try
	{
		libmaus2::util::ArgParser arg(argc,argv);

		if ( arg.uniqueArgPresent("v") || arg.uniqueArgPresent("version") )
		{
			std::cerr << "This is " << PACKAGE_NAME << " version " << PACKAGE_VERSION << "." << std::endl;
			std::cerr << PACKAGE_NAME << " is distributed under version 3 of the GPL." << std::endl;
			return EXIT_SUCCESS;
		}
		else if ( arg.uniqueArgPresent("h") || arg.uniqueArgPresent("help") || arg.size() < 3 )
		{
			std::cerr << "This is " << PACKAGE_NAME << " version " << PACKAGE_VERSION << "." << std::endl;
			std::cerr << PACKAGE_NAME << " is distributed under version 3 of the GPL." << std::endl;
			std::cerr << "\n";
			std::cerr << "usage: " << arg.progname << " [options] contigs.db reads.db in.las\n";
			std::cerr << "\n";
			std::cerr << "The following options can be used (no space between option name and parameter allowed):\n\n";
			std::cerr << helpMessage(arg);
			return EXIT_SUCCESS;
		}
		else
		{
			libmaus2::timing::RealTimeClock rtc;
			rtc.start();

			int r = EXIT_FAILURE;

			r = daccordcontig(arg);

			std::cerr << "[V] processing time " << rtc.formatTime(rtc.getElapsedSeconds()) << std::endl;

			return r;
		}
	}
	catch(std::exception const & ex)
	{
		std::cerr << ex.what() << std::endl;
		return EXIT_FAILURE;
	}
}
