/*
    daccord
    Copyright (C) 2018 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <libmaus2/util/ArgParser.hpp>
#include <libmaus2/dazzler/db/DatabaseFile.hpp>
#include <libmaus2/fastx/FastaPeeker.hpp>
#include <libmaus2/util/stringFunctions.hpp>
#include <libmaus2/math/IntegerInterval.hpp>
#include <libmaus2/lcs/NPLinMem.hpp>
#include <libmaus2/dazzler/align/OverlapIndexer.hpp>
#include <libmaus2/dazzler/align/AlignmentWriter.hpp>
#include <libmaus2/parallel/NumCpus.hpp>
#include <libmaus2/dazzler/align/SimpleOverlapParser.hpp>
#include <libmaus2/dazzler/align/OverlapDataInterface.hpp>
#include <libmaus2/dazzler/align/AlignmentWriterArray.hpp>
#include <libmaus2/dazzler/align/DalignerIndexDecoder.hpp>
#include <libmaus2/bambam/BamHeader.hpp>
#include <libmaus2/bambam/ProgramHeaderLineSet.hpp>
#include <libmaus2/bambam/BamAlignmentDecoderFactory.hpp>
#include <libmaus2/bambam/BamMultiAlignmentDecoderFactory.hpp>
#include <libmaus2/bambam/BamPeeker.hpp>
#include <libmaus2/bambam/BamBlockWriterBaseFactory.hpp>

#include <config.h>

static uint64_t getDefaultNumThreads()
{
	return libmaus2::parallel::NumCpus::getNumLogicalProcessors();
}

std::string getTmpFileBase(libmaus2::util::ArgParser const & arg)
{
	std::string const tmpfilebase = arg.uniqueArgPresent("T") ? arg["T"] : libmaus2::util::ArgInfo::getDefaultTmpFileName(arg.progname);
	return tmpfilebase;
}

struct SimpleThreadPoolTerminate
{
	libmaus2::parallel::SimpleThreadPool * STP;

	SimpleThreadPoolTerminate(libmaus2::parallel::SimpleThreadPool * rSTP)
	: STP(rSTP) {}

	~SimpleThreadPoolTerminate()
	{
		if ( STP )
		{
			STP->terminate();
			STP->join();
		}
	}
};

int64_t getRawId(std::string s)
{
	std::deque<std::string> tokens = libmaus2::util::stringFunctions::tokenize(s,std::string("/"));

	if ( 1 < tokens.size() )
	{
		std::istringstream istr(tokens[1]);
		int64_t i;
		istr >> i;

		if ( istr && istr.peek() == std::istream::traits_type::eof() )
			return i;
	}

	std::cerr << "[W] unable to parse " << s << std::endl;

	return -1;
}

int main(int argc, char * argv[])
{
	try
	{
		libmaus2::util::ArgParser const arg(argc,argv);

		// number of threads
		uint64_t const numthreads = arg.uniqueArgPresent("t") ? arg.getUnsignedNumericArg<uint64_t>("t") : getDefaultNumThreads();
		std::string const ref = arg.uniqueArgPresent("r") ? arg["r"] : std::string();

		std::vector<std::string> VR;
		if ( ref.size() )
		{
			libmaus2::fastx::FastAReader FA(ref);
			libmaus2::fastx::FastAReader::pattern_type pattern;
			for ( uint64_t i = 0; FA.getNextPatternUnlocked(pattern); ++i )
				VR.push_back(pattern.spattern);
		}

		std::string const tmpfilebase = getTmpFileBase(arg);

		libmaus2::util::ArgInfo arginfo(argc,argv);
		{
			std::ostringstream ostr;
			ostr << numthreads;
			arginfo.replaceKey("inputthreads",ostr.str());
			// arginfo.replaceKey("outputthreads",ostr.str());
		}

		libmaus2::parallel::SimpleThreadPool::unique_ptr_type PSTP;
		if ( numthreads > 1 )
		{
			libmaus2::parallel::SimpleThreadPool::unique_ptr_type TSTP(
				new libmaus2::parallel::SimpleThreadPool(numthreads)
			);
			PSTP = std::move(TSTP);

			libmaus2::bambam::BamAlignmentDecoderFactory::setThreadPool(PSTP.get());
		}

		SimpleThreadPoolTerminate STPT(PSTP ? PSTP.get() : 0);

		libmaus2::bambam::BamAlignmentDecoderWrapper::unique_ptr_type decwrapper(
			libmaus2::bambam::BamMultiAlignmentDecoderFactory::construct(
				arginfo,false, // do not put rank
				0, /* copy stream */
				std::cin, /* standard input */
				true, /* concatenate instead of merging */
				false /* streaming */
			)
		);
		::libmaus2::bambam::BamAlignmentDecoder * ppdec = &(decwrapper->getDecoder());
		::libmaus2::bambam::BamAlignmentDecoder & dec = *ppdec;
		// ::libmaus2::bambam::BamHeader const & header = dec.getHeader();

		#if 0
		std::string const headertext(header.text);

		// add PG line to header
		std::string const upheadtext = ::libmaus2::bambam::ProgramHeaderLineSet::addProgramLine(
			headertext,
			"bamfilterlongest", // ID
			"bamfilterlongest", // PN
			arg.commandline, // CL
			::libmaus2::bambam::ProgramHeaderLineSet(headertext).getLastIdInChain(), // PP
			std::string(PACKAGE_VERSION) // VN
		);
		// construct new header
		::libmaus2::bambam::BamHeader uphead(upheadtext);

		std::vector< ::libmaus2::lz::BgzfDeflateOutputCallback * > * Pcbs = 0;
		libmaus2::bambam::BamBlockWriterBase::unique_ptr_type Pwriter(
			libmaus2::bambam::BamBlockWriterBaseFactory::construct(uphead,arginfo,Pcbs));
		#endif

		// reads/103/0_9781
		char const * tag = "HP";

		uint64_t kepttrue = 0;
		uint64_t keptfalse = 0;
		uint64_t notkepttrue = 0;
		uint64_t notkeptfalse = 0;
		uint64_t nps = 0;
		uint64_t nnps = 0;

		libmaus2::bambam::BamPeeker BP(dec);
		libmaus2::bambam::BamAlignment algn;
		while ( BP.peekNext(algn) )
		{
			int64_t const refid = algn.getRefID();

			std::string const * r = (refid < static_cast<int64_t>(VR.size())) ? &VR[refid] : 0;

			std::vector < libmaus2::bambam::BamAlignment::shared_ptr_type > V;

			int64_t ps = -1;
			int64_t vid = -1;
			while ( BP.peekNext(algn) && algn.getRefID() == refid )
			{
				uint64_t const lvid = V.size();

				if ( r )
				{
					std::string const cigarstring = algn.getCigarString();
					std::vector<libmaus2::bambam::cigar_operation> const CV = libmaus2::bambam::CigarStringParser::parseCigarString(cigarstring);
					libmaus2::lcs::AlignmentTraceContainer ATC;
					libmaus2::bambam::CigarStringParser::cigarToTrace(CV.begin(),CV.end(),ATC,true /* ignore unknown */);
					std::string const bases = algn.getRead();
					uint64_t const fclip = algn.getFrontSoftClipping();

					bool const alok = libmaus2::lcs::AlignmentTraceContainer::checkAlignment(
						ATC.ta,ATC.te,
						r->begin() + algn.getPos(),
						bases.begin() + fclip
					);

					assert ( alok );

					// std::cerr << "alok=" << alok << " l=" << (ATC.te-ATC.ta) << std::endl;
				}

				BP.getNext(algn);
				libmaus2::bambam::BamAlignment::shared_ptr_type sptr(algn.sclone());
				V.push_back(sptr);

				int64_t const id = getRawId(algn.getName());

				if ( id == algn.getRefID() )
				{
					if ( algn.hasAux(tag) )
						ps = algn.getAuxAsNumber<int>(tag);
					vid = lvid;
				}
			}

			assert ( vid >= 0 );

			// std::cerr << "refid=" << refid << " ps=" << ps << std::endl;

			if ( ps >= 0 )
			{
				nps += 1;

				for ( uint64_t i = 0; i < V.size(); ++i )
					if ( static_cast<int64_t>(i) != vid )
					{
						if ( V[i]->hasAux(tag) )
						{
							int64_t const lps = V[i]->hasAux(tag) ? V[i]->getAuxAsNumber<int>(tag) : ps;
							bool const kept = lps == ps;
							bool const tr = V[i]->hasAux("tr") && V[i]->getAuxAsNumber<int>("tr");

							if ( tr )
							{
								if ( kept )
									kepttrue += 1;
								else
									notkepttrue += 1;
							}
							else
							{
								if ( kept )
									keptfalse += 1;
								else
									notkeptfalse += 1;
							}
						}
					}
			}
			else
			{
				nnps += 1;

				#if 0
				for ( uint64_t i = 0; i < V.size(); ++i )
					if ( static_cast<int64_t>(i) != vid )
					{
						bool const kept = true;
						bool const tr = V[i]->hasAux("tr") && V[i]->getAuxAsNumber<int>("tr");

						if ( tr )
						{
							if ( kept )
								kepttrue += 1;
							else
								notkepttrue += 1;
						}
						else
						{
							if ( kept )
								keptfalse += 1;
							else
								notkeptfalse += 1;
						}
					}
				#endif
			}
		}

		std::cout << "kepttrue\t" << kepttrue << std::endl;
		std::cout << "keptfalse\t" << keptfalse << std::endl;
		std::cout << "notkepttrue\t" << notkepttrue << std::endl;
		std::cout << "notkeptfalse\t" << notkeptfalse << std::endl;
		std::cout << "nps\t" << nps << std::endl;
		std::cout << "nnps\t" << nnps << std::endl;

		double const precision = static_cast<double>(kepttrue) / (kepttrue+keptfalse);
		double const recall = static_cast<double>(kepttrue) / (kepttrue + notkepttrue);
		double const fscore = 2 * precision * recall / (precision + recall);

		std::cout << "precision\t" << precision << std::endl;
		std::cout << "recall\t" << recall << std::endl;
		std::cout << "fscore\t" << fscore << std::endl;
	}
	catch(std::exception const & ex)
	{
		std::cerr << ex.what() << std::endl;
		return EXIT_FAILURE;
	}
}
